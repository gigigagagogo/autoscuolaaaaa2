<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Autoscuola Bararu</title>
    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon-32x32.png">
    <link rel="stylesheet" href="Home/image.css">
    <link rel="stylesheet" href="animazioni/animazione.css">
    <link rel="stylesheet" href="Home/nav.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>

    <div class="animazione">
        <span class="text1">Benvenuto nella</span>
        <span class="text2">AutoScuola Bararu</span>
    </div>


    <div class="container">
        <div class="inizio">
        <header id="prova">
             <img src="assets/AutoscuolaBararu.png" alt="Logo" width="20%" class="logo" onclick="src"/>
            <div class="mauro">
                <div class="simbolo" style="display: flex; justify-content: center">
                    <ion-icon style="width: 60px;height: 60px;" name="person"></ion-icon>
                </div>
                <div class="bottoni">
                    <a href="admin/login/login2.php"><button style="margin-bottom: 3px" class="Contact">Login</button></a>
                    <a href="admin/register/register4.php"><button class="Contact">Register</button></a>
                </div>
            </div>
        </header>
        </div>
        <br>
        <br>
        <br>
        <br>

<?php
require_once "config.php";

try {
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}



?>
<img class="macchinar" src="assets/macchinina.png"/>
        <div class="center" id="motivazionale">
            <label style="font-family: 'Dancing Script', cursive;">"Non importa cosa c'è sotto il cofano, importa chi c'è dietro al volante!"</label>
        </div>

        <div class="rettangolor"><a href="Home/Contattaci.php"><h1><label>Dove trovarci</label></h1></a></div>

        <div class="rettangoloblu">
            <img style="box-shadow: 5px 6px 5px #ccc;" src="assets/tipa.png"/>
            <div class="testo">
                <label style="font-family: 'Gentium Plus', serif;font-size: 19px;">
                    Leader nel settore delle patenti dal 1970, vanta un’alta <br>
                    professionalità grazie al Know-how acquisito negli anni e <br>
                    all’esperienza dei propri insegnanti. Dalle lezioni teoriche <br>
                    in aula alle esercitazioni su computer fino alle guide con<br>
                    istruttore di scuola guida e il conseguimento dell’abilitazione.<br>
                </label>
            </div>
        </div>

        <div class="rettangolo3">
            <div class="testo2">
                <label style="font-family: 'Gentium Plus', serif;font-size: 19px;">
                    Lasciati "guidare" dalla nostra esperienza,<br>
                    impara in modo consapevole i principi teorici<br>
                    e pratici di un buon guidatore. L'autoscuola Bararu<br>
                    a Castiglione D/S organizza corsi di preparazione<br>
                    agli esami teorici. Vengono organizzati corsi pratici<br>
                    di guida e tutte le pratiche necessarie per<br>
                    chi deve prendere la patente
                </label>
            </div>
                <img style="box-shadow: 5px 6px 5px #ccc;" src="assets/2im.png"/>
        </div>

        <div class="rettangoloblu">
            <img style="box-shadow: 5px 6px 5px #ccc;" src="assets/tab.png"/>
            <div class="testo">
                    <label style="font-family: 'Gentium Plus', serif;font-size: 19px;">
                        Possibilitò di partecipazione online e in presenza, <br>
                        lezioni online registrabili per rivederle tutte le volte<br>
                        che si vuole, applicazione dedicata per ogni studente<br>
                        con E-book e esercitazioni sui quiz.<br>
                    </label>
            </div>
        </div>

        <div class="rettangolob"><h1><label>Contattaci al : +39 351 688 3870</label></h1></div>

        <div class="footer">
            <div class="sinistra">
                <h5>
                Contattaci info@brubi.it | +39 351 688 3870<br>
                Lavora con noi AutoscuolaBararu@brubi.it<br>
                Sede Operativa Cesare Battisti 34, Castiglione delle Stiviere(MN)
                    </h5>
            </div>

            <div class="destra">
                <h5>
                    Autoscuole Bararu<br>
                    C.F e P.IVA 03709232562<br>
                    ©2022 Copyright<br>
                    Privacy Policy Cookie Policy
                </h5>
            </div>
        </div>


</div>


</body>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</html>