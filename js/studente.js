
window.onload = () =>  {
    document.getElementById("image").addEventListener("change", (e) => {
        let file = e.target['files'][0];
        let reader = new FileReader();
        reader.onloadend = function () {
            document.getElementById("pfp-img").src = reader.result;
        };
        reader.readAsDataURL(file);
    });
}