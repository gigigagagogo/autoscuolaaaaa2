<?php
require "../../config.php";
require_once "../../authorized.php";
verify('Docente' );

$id = intval($_GET['id']) ?? 0;
$pag = intval($_GET['pag'] ?? '0'); #pagina
$NRP = 6;  #numero di record che fa vedere su una pagina
$offset = $pag * $NRP;

$q = $_GET['q'] ?? '';
$order = $_GET['order'] ?? '';
if(!in_array($order, ['', 'nome', 'cognome', 'patente'])) {
    $order = '';
}


try {
    $sql="
    SELECT SQL_CALC_FOUND_ROWS 
        I.id,I.nome,I.cognome,I.id_patente,P.patente
    FROM iscritti I 
    left join users U on U.id=I.id_users 
    left join patenti P on I.id_patente=P.id
    where role='Studente' 
    ";

    $sql .= "GROUP BY I.id ";
    if($q != '') {
        $sql .= "HAVING I.nome LIKE :q 
        OR I.cognome LIKE :q 
        OR P.patente LIKE :q 
        ";
    }

    if($order != '') {
        $sql .= "ORDER BY $order ASC";
    }
    $sql .= " LIMIT $offset, $NRP"; #estrare un frammento della tabella

    $stmt = $db->prepare($sql);
    if($q != '') {
        $stmt->bindValue(":q", "%$q%");
    }
    $stmt->execute();
    #$stmt->debugDumpParams(); // DEBUG

    $res = $db->query("SELECT FOUND_ROWS() AS TREC", PDO::FETCH_ASSOC);
    $TREC = intval($res->fetch()['TREC']);
    #var_export($TREC);

    $stmt->execute();
    #$stmtb->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}



?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Amministrazione docenti</title>
    <link rel="stylesheet" href="../tabella.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">



</head>
<body >
<a href="../profili/profilo.php?id=<?= $id ?>"><ion-icon class="profilo" name="person-circle-outline"></ion-icon></a>



<div>
    <br>
    <!--<div class="center"><img src="../assets/Scuola-guida-mascherina.png" alt="Mia immagine" style="..." class="center"</img></div>-->

    <div class="center">
    <div class="top">
        <a class="icone" href="../amministrazione/admin.php"><img src="../../assets/porta.png"></a>
        <label class="intro">Amministrazione Studenti</label>
        <a  href="../logged/home.php?id=<?= $id ?>"><ion-icon class="icone" name="home"></ion-icon></a>
    </div>

  <a href="add.php" style="z-index: 1"><span class="material-icons" style="color: black">add_circle_outline</span></a>

    <form method="get" id="qform" style="z-index: 1;top: 300px;position: absolute;">
        <input style="width: 200px;height: 25px;" placeholder="Search..." name="q" id="q" value="<?= $q ?>"></label>
        <button><ion-icon style="height: 25px;width: 25px" name="search-outline"></ion-icon></button>
        <button onclick="this.form.q.value=''">X</button>
        <input type="hidden" name="order" value="<?= $order ?>">
        <input type="hidden" name="pag" value="<?= $pag ?>">

    </form>

        <table>
            <tr>
                <th>id</th>
                <th>Studenti</th>
                <th>Patenti</th>
                <th></th>
            </tr>



            <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>

                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['nome'] ?> <?= $row['cognome'] ?></td>
                    <td><?= $row['patente'] ?></td>


                    <td>
                        <button onclick="mod(<?= $row['id'] ?>)"><span class="material-icons" style="cursor: pointer">edit</span></button>
                        <button onclick="del(<?= $row['id'] ?>)"><span class="material-icons" style="cursor: pointer">delete</span></button>
                    </td>
                </tr>
            <?php endwhile ?>

        </table>
        <div style="text-align: center;top: 610px;position: absolute; ">
            <button onclick="goto(0)">|&lt;</button>
            <button onclick="go(-1)">&lt;</button>
            <button onclick="go(+1)">&gt;</button>
            <button onclick="goto(<?= ceil($TREC / $NRP) - 1 ?>)">&gt;|</button>
        </div>
    </div>
</div>
    <script>
        function del(id) {
            if (confirm('Sei sicuro si voler eliminare questo docente?')) {
                location = "del.php?id=" + id ;
            }
        }

        function mod(id) {
            location = "edit.php?id=" + id;
        }
        function mod(id) {
            location = "edit.php?id=" + id;
        }
        function go(diquant) {
            let LASTPAGE = <?= ceil($TREC / $NRP) - 1 ?>;
            let p = diquant + 1 * document.getElementById('qform').pag.value
            if(p < 0) p = 0
            if (p > LASTPAGE) p = LASTPAGE;
            document.getElementById('qform').pag.value = p
            document.getElementById('qform').submit()
        }
        function goto(pag) {
            document.getElementById('qform').pag.value = pag
            document.getElementById('qform').submit()
        }

        function sortby(field) {
            document.getElementById('qform').order.value = field
            document.getElementById('qform').submit()
        }
    </script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</body>
</html>