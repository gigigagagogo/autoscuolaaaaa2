<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../admin.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">
    <title>Modifica Studente</title>
    <style>
        label {
            width: 4pc;
            display: inline-block;
        }

        input[value="Salva"] {
            background-color: yellowgreen;

        }

        input[type=submit], input[type=button], input[type=reset] {
            cursor: pointer;
            border: 1px solid #4444;
            border-radius: 2px;
        }
    </style>
</head>
<body>

<?php
require "../../config.php";
require_once "../../config.php";
require_once "../../authorized.php";
verify('Admin' || 'Docente');



$id = intval($_GET['id']) ?? 0;
#var_export($id);
try {

    $stmtb = $db-> prepare("
    SELECT I.nome,I.cognome,I.id_patente 
    FROM iscritti I 
    left join patenti P on P.id=I.id_patente
    where I.id=:id
    ");

    $stmtb->bindParam(":id", $id);
    $stmtb->execute();
    $author = $stmtb->fetch(PDO::FETCH_ASSOC);

    $stmt = $db-> prepare("
    SELECT * FROM patenti P
    ");
    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


if (isset($_SESSION['add_data'])) {
    $msg = $_SESSION['add_data']['msg'];
    $nome = $_SESSION['add_data']['nome'];
    $cognome = $_SESSION['add_data']['cognome'];
    $patente = $_SESSION['add_data']['patente'];
    #$id_patente = $_SESSION['add_data']['id_patente'];
    unset($_SESSION['add_data']);
} else {
    $msg = '';
    $nome = $author['nome'];
    $cognome = $author['cognome'];
    $patente = $author['patente'];
    #$id_patente = $author['id_patente'];
}
?>


<h2>Modifica Studente</h2>

<?php if($msg != ''): ?>
    <div class="error"><?= $msg?> </div>
<?php endif ?>
<br>

<form method="post" action="edit_r.php" enctype="multipart/form-data">

    <label for="nome">Nome</label>
    <input id="nome" type="text" name="nome" size="30" maxlength="255" value="<?= $nome ?>">
    <br><br>

    <label for="cognome">Cognome</label>
    <input id="cognome" type="text" name="cognome" size="30" maxlength="255" value="<?= $cognome ?>">
    <br><br>

    <table>
    <tr>
        <td>Patente:</td>
        <td>
            <select name="id_patente" id="id_patente">
                <!--<option selected hidden>Seleziona una patente</option>-->
                <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
                    <?php $sel = ($author['id_patente'] === $row['id']) ? 'selected' : '';  ?>
                    <option value="<?= $row['id'] ?>" <?= $sel ?>><?= $row['patente'] ?></option>
                <?php endwhile ?>
            </select>
        </td>
    </tr>
    </table>
    <br>

    <input hidden id="id" name="id" type="number" value="<?= $id ?>">


    <input type="button" value="Annulla" onclick="history.back()">
    <input type="reset">
    <input type="submit" value="Salva">


</form>

</body>
</html>