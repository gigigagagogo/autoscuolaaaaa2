<?php
require "../../config.php";
require_once "../../authorized.php";
verify('Admin');

$pag = intval($_GET['pag'] ?? '0'); #pagina
$NRP = 4;  #numero di record che fa vedere su una pagina
$offset = $pag * $NRP;

$q = $_GET['q'] ?? '';
$order = $_GET['order'] ?? '';
if(!in_array($order, ['', 'patente', 'giorno'])) {
    $order = '';
}

try {
    $sql="
    SELECT  SQL_CALC_FOUND_ROWS  
        P.id,P.patente,group_concat(G.giorno SEPARATOR ',')  as giorni from patenti P
        join giorni_patenti GP on P.id=GP.id_patente
        join giorni G on GP.id_giorno=G.id
   
    ";


    $sql .= "GROUP BY P.id ";
    if($q != '') {
        $sql .= "HAVING P.patente LIKE :q 
        OR giorni LIKE :q 
        
        ";
    }

    if($order != '') {
        $sql .= "ORDER BY $order ASC";
    }
    $sql .= " LIMIT $offset, $NRP"; #estrare un frammento della tabella

    $stmt = $db->prepare($sql);
    if($q != '') {
        $stmt->bindValue(":q", "%$q%");
    }
    $stmt->execute();
    #$stmt->debugDumpParams(); // DEBUG

    $res = $db->query("SELECT FOUND_ROWS() AS TREC", PDO::FETCH_ASSOC);
    $TREC = intval($res->fetch()['TREC']);

    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title >Amministrazione docenti</title>
    <link rel="stylesheet" href="../tabella.css">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


</head>
<div>






    <div class="center" style="margin-top: 50px" >
        <div class="top">
            <a  href="../amministrazione/admin.php"><ion-icon class="icone" name="arrow-back-outline"></ion-icon></a>
            <label class="intro">Amministrazione Patenti</label>
            <a  href="../logged/home.php"><ion-icon class="icone" name="home"></ion-icon></a>
        </div>
    <a href="add.php" style="z-index: 1"><ion-icon style="height: 30px;width: 30px;"  id="agg" name="add-circle-outline"></ion-icon></a>

    <form method="get" id="qform" >
        <div class="ricerca">
            <input style="width: 200px;height: 28px;" placeholder="Search..." name="q" id="q" value="<?= $q ?>"></label>
            <button><ion-icon style="height: 25px;width: 25px;cursor: pointer;" name="search-outline"></ion-icon></button>
            <button onclick="this.form.q.value=''"><ion-icon style="height: 25px;width: 25px;cursor: pointer;" name="close-outline"></ion-icon></button>
        </div>
        <input type="hidden" name="order" value="<?= $order ?>">
        <input type="hidden" name="pag" value="<?= $pag ?>">

        <!-- <input type="button" onclick="this.form.q.value='';this.form.submit()" value="X">-->
    </form>
    </div>

        <table>
            <tr>
                <th>Patenti</th>
                <th>Giorni</th>
                <th></th>
            </tr>


            <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>

                <tr>

                    <td><?= $row['patente'] ?></td>
                    <td><?= $row['giorni'] ?></td>

                    <td>
                        <button onclick="mod(<?= $row['id'] ?>)"><span style="cursor: pointer;" class="material-icons">edit</span></button>
                        <button onclick="del(<?= $row['id'] ?>)"><span style="cursor: pointer;" class="material-icons">delete</span></button>
                    </td>
                </tr>
            <?php endwhile ?>

        </table>

        <div id="dir">
            <button onclick="goto(0)"><ion-icon style="height: 25px;width: 25px;cursor: pointer;" name="caret-back-circle-outline"></ion-icon></button>
            <button onclick="go(-1)"><ion-icon style="height: 25px;width: 25px;cursor: pointer;"name="chevron-back-circle-outline"></ion-icon></button>
            <button onclick="go(+1)"><ion-icon style="height: 25px;width: 25px;cursor: pointer;" name="chevron-forward-circle-outline"></ion-icon></button>
            <button onclick="goto(<?= ceil($TREC / $NRP) - 1 ?>)"><ion-icon style="height: 25px;width: 25px;cursor: pointer;" name="caret-forward-circle-outline"></ion-icon></button>
        </div>
    </div>

    <script>
        function del(id) {
            if (confirm('Sei sicuro si voler eliminare questo docente?')) {
                location = "del.php?id=" + id ;
            }
        }

        function mod(id) {
            location = "edit.php?id=" + id;
        }

        function go(diquant) {
            let LASTPAGE = <?= ceil($TREC / $NRP) - 1 ?>;
            let p = diquant + 1 * document.getElementById('qform').pag.value
            if(p < 0) p = 0
            if (p > LASTPAGE) p = LASTPAGE;
            document.getElementById('qform').pag.value = p
            document.getElementById('qform').submit()
        }
        function goto(pag) {
            document.getElementById('qform').pag.value = pag
            document.getElementById('qform').submit()
        }

        function sortby(field) {
            document.getElementById('qform').order.value = field
            document.getElementById('qform').submit()
        }
    </script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</body>
</html>