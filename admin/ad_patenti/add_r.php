<?php


require "../../config.php";

#var_export($_POST); die;

$patente = $_POST['patente'] ?? '';
$giorni_id = $_POST['giorni_id'] ?? [];


//var_dump($year);

if ($patente == '' ) {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'msg' => 'Some required data is missing',
        'patente' => $patente,
        'giorni_id' =>$giorni_id


    ];
    header('location: /admin/ad_patenti/add.php?');
    die;
}

try {
    $stmt = $db-> prepare("
        INSERT INTO patenti SET 
            patente = :patente
    ");

    $stmt->bindParam(':patente', $patente);
    $stmt->execute();

    $id_patente = $db->lastInsertId();

    $stmt = $db-> prepare("
        INSERT INTO giorni_patenti SET
            id_patente=:id_patente, 
            id_giorno=:id_giorno
    ");

    $stmt->bindParam(':id_patente', $id_patente);

    foreach ($giorni_id as $giorni) {
        $stmt->bindParam(':id_giorno', $giorni, PDO::PARAM_INT);
        $stmt->execute();
    }

}catch (PDOException $e) {

    echo "Errore: " . $e->getMessage();
    die();

}

header('location: /admin/ad_patenti/ad_patente.php');


?>




