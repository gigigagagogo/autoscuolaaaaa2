<?php
require "../../config.php";
require_once "../../config.php";
require_once "../../authorized.php";
verify('Admin');
try {
    $stmta = $db-> prepare("SELECT * FROM giorni");
    $stmta->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

if (isset($_SESSION['add_data'])) {
    $msg = $_SESSION['add_data']['msg'];
    $patente = $_SESSION['add_data']['patente'];
    $giorni_id = $_SESSION['add_data']['giorni_id'];

    unset($_SESSION['add_data']);

} else {
    $msg = '';
    $patente = '';
    $giorni_id = [];
}
?>


<?php if($msg != ''): ?>
    <div class="error"><?= $msg?> </div>
<?php endif ?>

<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../aggiunta.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">
    <title>Nuova Patente</title>

</head>
<body>


<br>
<form method="post" action="add_r.php" enctype="multipart/form-data">
    <div class="center">
        <label class="intro">Nuova Patente</label>
        <div class="contenuto">
            <div class="inff">
                <label style="margin-top: 100px" class="info" for="patente">Patente:<input placeholder="Patente" class="inser" id="patente" type="text" name="patente" size="20" maxlength="255" value="<?= $patente ?>"></label>



                <label class="info" for="giorni">Giorni:
                    <select name="giorni_id[]" id="giorno_id" multiple>
                        <?php while($row = $stmta->fetch(PDO::FETCH_ASSOC)): ?>
                    <option <?= in_array($row['id'], $giorni_id) ? 'selected' : '' ?> value="<?= $row['id'] ?>"><?= $row['giorno'] ?></option>
                    <?php endwhile ?>
                    </select>
                </label>
                <div style="margin-bottom: 50px;margin-top: 50px;gap: 20px"" class="bottoni">
                    <input class="btn" type="button" value="Annulla" onclick="history.back()">
                    <input class="btn" type="reset">
                    <input class="btn" type="submit" value="Salva">
                </div>
            </div>
        </div>
    </div>
</form>

</body>
</html>