<?php

require "../../config.php";

#echo '<pre>';  var_export($_POST); var_export($_FILES); die;

#var_export($_POST);die();
$patente = $_POST['patente'] ?? '';
$giorni_id = $_POST['giorni_id'] ?? [];
$id = $_POST['id'] ?? 0;

if ($id == '') $id = 0;

//var_dump($year);

if ($patente == '' ) {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'msg' => 'Some required data is missing',
        'patente' => $patente,
        'giorni_id' => $giorni_id
    ];
    header('location: /admin/ad_patenti/edit.php?id=' . $id);
    die;
}


try {

    $stmt = $db-> prepare("
       UPDATE patenti SET
        patente = :patente
                      
        where id = :id_patente
    ");

    $stmt->bindParam(':patente', $patente);
    $stmt->bindParam(':id_patente', $id);

    $stmt->execute();

    $stmt = $db-> prepare("
        DELETE FROM giorni_patenti where id_patente = :id_patente
    ");
    $stmt->bindParam(':id_patente', $id);
    $stmt->execute();



    $stmt = $db-> prepare("
        INSERT INTO giorni_patenti VALUES (
            :id_patente, :id_giorno
        )
    ");

    $stmt->bindParam(':id_patente', $id);
    foreach ($giorni_id as $giorni) {
        $stmt->bindParam(':id_giorno', $giorni, PDO::PARAM_INT);
        $stmt->execute();
    }

    # SAVE THE PICTURE TOO



}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/ad_patenti/ad_patente.php');




?>



