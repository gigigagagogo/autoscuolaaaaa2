<?php
require_once "../../config.php";
require_once "../../authorized.php";
verify('Admin');
#var_export($_SESSION['user']);
?>
<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" a href="amministrazione.css"/>
    <title>Admin</title>
</head>
<body>

    <div class="top">
            <a class="link" href="../logout.php"><img style="width: 40px;height: 40px;" src="../../assets/porta.png"></a>

         <label class="benvenuto"><?php echo 'Welcome '.$_SESSION['username']; ?></label>

            <a  class="link" href="../logged/home.php"><ion-icon name="home" style="width: 40px;height: 40px;color: black;"  ></ion-icon></a>
    </div>

<div class="mezzo">
    <div class="container">
        <ion-icon id="icons" name="person"></ion-icon>
        <a href="../ad_docente/ad_docente.php" class="link"><label class="info" >Docenti</label></a>
    </div>

    <div class="container2">
        <ion-icon id="icons" name="school"></ion-icon>
        <a href="../ad_studente/ad_studente.php" class="link"><label class="info" >Studenti</label></a>
    </div>

    <div class="container3">
        <ion-icon id="icons" name="car-sport"></ion-icon>
        <a href="../ad_patenti/ad_patente.php" class="link"><label class="info" >Patenti</label></a>
    </div>
</div>

</body>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</html>
