const rmCheck = document.getElementById("rememberMe"),
    usernameInput = document.getElementById("username"),
    passwordInput = document.getElementById("password");

if (localStorage.checkbox && localStorage.checkbox !== "") {
    rmCheck.setAttribute("checked", "checked");
    usernameInput.value = localStorage.username;
    passwordInput.value = localStorage.username;
} else {
    rmCheck.removeAttribute("checked");
    usernameInput.value = "";
    passwordInput.value = "";
}

function lsRememberMe() {
    if (rmCheck.checked && usernameInput.value !== "") {
        localStorage.username = usernameInput.value;
        localStorage.password = passwordInput.value;
        localStorage.checkbox = rmCheck.value;
    } else {
        localStorage.username = "";
        localStorage.password = "";
        localStorage.checkbox = "";

    }
}