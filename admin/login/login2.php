<?php
require_once "../../config.php";
if(!isset($_SESSION['backto'])){
    $backto=$_SERVER['HTTP_REFERER'] ?? '';
    if($backto==''){
        $backto='login2.php';
    }
    $_SESSION['backto']=$backto;
}
?>

<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="login2.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>

<div class="container">
        <nav>
            <ul class="menu">
                <li class="has-children"><a href="#"><span class="material-icons" >menu</span></a>
                <ul class="sub-menu">
                    <li><a href="../../index.php">Home</a></li>
                    <li><a href="../../Home/Contattaci.php">Contattaci</a></li>
                    <li><a href="../../admin/register/register4.php">Register</a></li>
                </ul>
                </li>
                <!--<h3 >Autoscuole<br>Bararu</h3>-->
            </ul>
        </nav>
    <div class="mezzo">
        <form method="post" action="login_r.php" class="form">
            <div class="Customer">
                <h4>CUSTOMER<br>LOGIN</h4>
            </div>
            <div class="contenuto">
                <div class="form_input username">
                    <span style="color: black;" class="material-icons" id="omino" >person</span>
                    <input class="field" type="text" id="username" name="username" placeholder="Username" size="30" style="outline: none;">
                </div>

                <br>

                <div class="form_input username">
                    <span style="color: black; " class="material-icons" id="lucchetto">lock</span>
                    <input class="field" type="password" id="password" name="password" placeholder="Password" size="30" style="outline: none;">
                </div>
                <a class="pass" style="text-decoration: none" href="../pass.php"><label class="dpass">Hai dimenticato la password?</label></a>
                <br>
                <div class="elememts" style="margin-top: 25px;">
                    <div class="check">
                        <input type="checkbox" value="lsRememberMe" id="rememberMe"> <label for="rememberMe">Remember me</label>
                    </div>

                    <div class="log" style="font-weight: bold;">
                        <input  style="cursor: pointer" class="btn-login" type="submit" value="Login" onclick="lsRememberMe()"/>
                    </div>
                </div>
                <br><br>

            </div>
        </form>
    </div>


</div>
</body>
    <script src="remember.js"></script>

</html>

