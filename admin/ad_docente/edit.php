<?php
require "../../config.php";
require_once "../../authorized.php";
verify('Admin');

$id = intval($_GET['id']) ?? 0;

try {

    $stmtb = $db-> prepare("
    SELECT I.id,I.nome, I.cognome,I.id_patente,U.username
    FROM iscritti I
    LEFT JOIN patenti P ON P.id = I.id_patente
    LEFT JOIN users U ON U.id=I.id_users
    WHERE I.id = :id
    ");
    $stmtb->bindParam(":id", $id);
    $stmtb->execute();
    $author = $stmtb->fetch(PDO::FETCH_ASSOC);

    $stmt = $db-> prepare("
    SELECT * FROM patenti P
    ");
    $stmt->execute();


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


if (isset($_SESSION['add_data'])) {
    $msg = $_SESSION['add_data']['msg'];
    $username = $_SESSION['add_data']['username'];
    $nome = $_SESSION['add_data']['nome'];
    $cognome = $_SESSION['add_data']['cognome'];
    unset($_SESSION['add_data']);
} else {
    $msg = '';
    $username = $author['username'];
    $nome = $author['nome'];
    $cognome = $author['cognome'];
}
?>

<?php if($msg != ''): ?>
    <div class="error"><?= $msg?> </div>
<?php endif ?>

<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../aggiunta.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">
    <title>Modifica docente</title>

</head>
<body>





<form method="post" action="edit_r.php" enctype="multipart/form-data">

    <div class="center">

        <label class="intro">Modifica docente</label>

        <div class="contenuto">
            <div class="inff">
                <label style="margin-top: 100px" class="info" for="username">Username:<input class="inser" placeholder="Username" id="username" type="text" name="username" size="20" maxlength="255" value="<?= $username ?>"></label>



                <label class="info" for="nome">Nome:<input class="inser" placeholder="Nome" id="nome" type="text" name="nome" size="20" maxlength="255" value="<?= $nome ?>"></label>



                <label class="info" for="cognome">Cognome:<input class="inser" placeholder="Cognome" id="cognome" type="text" name="cognome" size="20" maxlength="255" value="<?= $cognome ?>"></label>



                <label class="info">Patente:
                    <select name="id_patente" id="id_patente">
                        <!--<option selected hidden>Seleziona una patente</option>-->
                        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
                            <?php $sel = ($author['id_patente'] === $row['id']) ? 'selected' : '';  ?>
                            <option value="<?= $row['id'] ?>" <?= $sel ?>><?= $row['patente'] ?></option>
                        <?php endwhile ?>
                    </select>
                </label>


                <div style="margin-bottom: 50px;margin-top:50px;gap: 20px" class="bottoni">
                    <input class="btn" type="button" value="Annulla" onclick="history.back()">
                    <input class="btn" type="reset">
                    <input class="btn" type="submit" value="Salva">
                </div>
            </div>

        </div>
    </div>
    <input hidden id="id" name="id" type="number" value="<?= $id ?>">
</form>

</body>
</html>