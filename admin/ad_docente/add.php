<?php
require "../../config.php";
require_once "../../config.php";
require_once "../../authorized.php";
verify('Admin');
try {
    $stmta = $db-> prepare("SELECT * FROM patenti");
    $stmta->execute();


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

if (isset($_SESSION['add_data'])) {
    $msg = $_SESSION['add_data']['msg'];
    $username = $_SESSION['add_data']['username'];
    $nome = $_SESSION['add_data']['nome'];
    $cognome = $_SESSION['add_data']['cognome'];
    #$patente = $_SESSION['add_data']['patente'];
    $password = $_SESSION['add_data']['password'];

    unset($_SESSION['add_data']);

} else {
    $msg = '';
    $username = '';
    $nome = '';
    $cognome = '';
    #$patente = '';
    $password = '';
}
?>

<?php if($msg != ''): ?>
    <div class="error"><?= $msg?> </div>
<?php endif ?>

<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../aggiunta.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">
    <title>Nuovo Docente</title>

</head>
<body>


<br>
<form method="post" action="add_r.php" enctype="multipart/form-data">

    <div class="center">

        <label class="intro" >Nuovo Docente</label>
        <div class="contenuto">
            <div class="inff">
                <label style="margin-top: 100px;" class="info" for="username">Username:<input placeholder="Username" class="inser" id="username" type="text" name="username" size="20" maxlength="255" value="<?= $username ?>"></label>



                <label class="info" for="nome">Nome:<input placeholder="Nome" class="inser" id="nome" type="text" name="nome" size="20" maxlength="255" value="<?= $nome ?>"></label>



                <label class="info" for="cognome">Cognome:<input placeholder="Cognome" class="inser" id="cognome" type="text" name="cognome" size="20" maxlength="255" value="<?= $cognome ?>"></label>

                <label class="info" >Patente:
                    <select name="id_patente" id="id_patente">
                        <option  selected hidden>Seleziona una patente</option>
                        <?php while($row = $stmta->fetch(PDO::FETCH_ASSOC)): ?>
                            <option   value="<?= $row['id'] ?>"><?= $row['patente'] ?></option>
                        <?php endwhile ?>
                    </select>
                </label>


                <label style="margin-bottom: 60px" class="info" for="password">Password:<input placeholder="Password" class="inser"  id="password" type="password" name="password" size="20" maxlength="255" value="<?= $password ?>"></label>


            <div style="margin-bottom: 50px;gap: 20px" class="bottoni" >
                <input class="btn"  type="button" value="Annulla" onclick="history.back()">
                <input class="btn"  type="reset">
                <input class="btn"  type="submit" value="Salva">
            </div>
            </div>
        </div>
    </div>









</form>

</body>
</html>