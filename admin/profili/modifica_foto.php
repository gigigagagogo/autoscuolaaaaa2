<?php
require_once "../../config.php";

$id = intval($_GET['id'] ?? 0);

$nome = $_POST['nome'] ?? '';
$cognome = $_POST['cognome'] ?? '';

if ($nome == '' || $cognome == "") {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'msg' => 'Some required data is missing',
        'nome' => $nome,
        'cognome' => $cognome
    ];


}

try {

    $stmt = $db-> prepare("
       UPDATE iscritti SET
        nome = :nome,
        cognome = :cognome
        where id = :id
    ");

    $stmt->bindParam(':nome', $nome);
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':cognome', $cognome);

    $stmt->execute();

    if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
        var_export(move_uploaded_file($_FILES['image']['tmp_name'], "../foto/$id.png"));
    }

    header('location: /admin/profili/profilo.php?id=' . $id);

} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

$row = $stmt->fetch(PDO::FETCH_ASSOC);
#var_export($_SESSION);
#die();
?>
<html>

<head>
    <link rel="stylesheet" href="studente.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="../../main.js"></script>
</head>

<body class="profilo">


<div class="card" style="background-color: lavenderblush">

    <div style="display: flex;">
        <div class="inserimento">
            <span class="material-icons" style="position: absolute; right: 438px;top:370px;z-index:1;">image</span><input id="image" name="imag" class="file" type="file" onchange="select_image(this);"><label class="inserisci">Inserisci immagine</label></input>
        </div>
        <span class="material-icons" style="position: relative; left: 29px; top: 25px;z-index:1">delete</span><button class="rimuovi">Cancella l'immagine</button>
    </div>

    <h1><?php echo "Bentornato " . $_SESSION['username'] ?></h1>

    <p class="title">Studente</p>
    <div style="display: flex;flex-direction: row; align-items: center">
        <label style="margin-right: 150px">nome:</label>
        <h3 class="nome"><?= $_SESSION['user']['nome'] ?></h3>
    </div>

    <div style="display: flex;flex-direction: row; align-items: center">
        <label style="margin-right: 150px">Cognome:</label>
        <h3 class="cognome"><?= $_SESSION['user']['cognome'] ?></h3>
    </div>

    <div style="display: flex;flex-direction: row; align-items: center">
        <label style="margin-right: 150px">Patente:</label>
        <h3 class="patente"><?= $row['patente'] ?></h3>
    </div>

</div>
<input type="button" value="Annulla" onclick="history.back()">
<input type="reset">
<input type="submit" value="Salva">
</body>

</html>