<?php
require_once "../../config.php";
require_once "../../authorized.php";
verify('Docente' || 'Studente');

$id = intval($_GET['id']) ?? 0;


try {
    $stmt = $db->prepare("
    SELECT U.username,I.nome,I.cognome,P.patente as patente,U.role FROM iscritti I 
    left join patenti P on P.id=I.id_patente
    left join users U on U.id=I.id_users
    where I.id=:id
    ");
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    $profilo = $stmt->fetch(PDO::FETCH_ASSOC);

} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

$row = $stmt->fetch(PDO::FETCH_ASSOC);
#var_export($_SESSION);
#die();
?>
<html>

<head>
    <link rel="stylesheet" href="profilo.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="../../main.js"></script>
</head>

<body class="profilo">

<form method="post" action="profilom.php?id=<?= $id ?>" enctype="multipart/form-data">

    <nav class="nav">
        <ul class="menu">
            <li class="has-children"><a href="#"><span class="material-icons" >menu</span></a>
                <ul class="sub-menu" style="padding: 0">
                    <li><a href="../logged/home.php?id=<?= $id ?>">Home</a></li>
                    <li><a href="../../Home/Contattaci.php">Contattaci</a></li>
                    <li><a href="../logout.php">Esci</a></li>
                </ul>
            </li>
            <?php if($_SESSION['user']['role']=='Docente'):?>
                <a href="../docente/docente.php?id=<?= $id ?>"><ion-icon style="opacity: 1;color: black;" name="arrow-back"></ion-icon></a>
            <?php else:?>
                <ion-icon style="opacity: 0" name="arrow-back"></ion-icon>
            <?php endif ?>
            <!--<h3 >Autoscuole<br>Bararu</h3>-->
        </ul>
    </nav>


    <div class="card" >
        <div class="colore">
            <div class="immagine">
                <?php if(file_exists("../foto/$id.png")): ?>
                    <img  class="img" id="pfp-img" src="../foto/<?= $id ?>.png" height="200px">
                <?php else: ?>
                    <img  class="img" src="../foto/profilo2.png" height="200px">
                <?php endif ?> &nbsp; &nbsp;
            </div>
            <h1><?php /*echo "Bentornato " . $profilo['username']*/ ?></h1>


            <label class="title"><?= $profilo['role'] ?></label>

        </div>

        <div class="colors">
            <div class="contenuto">
                <label style="font-size: 27px;">Nome:<?=$profilo['nome']?>  </label>


                <label style="font-size: 27px;">Cognome:<?=$profilo['cognome']?></label>


                <label style="font-size: 27px;">Patente:<?=$profilo['patente']?></label>
            </div>


            <div class="bottone">
                <a href="studentem.php?id=<?= $id ?>"><button class="btn" style="padding: 15px 25px;">Modifica</button></a>
            </div>
        </div>

    </div>


</form>
</body>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</html>