<?php
require_once "../../config.php";
require_once "../../authorized.php";
verify('Docente' || 'Studente');

$id = intval($_GET['id']) ?? 0;

try {
    $stmt = $db->prepare("
    SELECT I.nome,I.cognome,p.patente 
    FROM iscritti I 
    join users U on U.id = I.id_users
    join patenti p on p.id = I.id_patente 
    where I.id=:id 
    ");

    $stmt->bindParam(":id", $id);
    $stmt->execute();
    $profilo = $stmt->fetch(PDO::FETCH_ASSOC);


} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}



if (isset($_SESSION['add_data'])) {
    $msg = $_SESSION['add_data']['msg'];
    $nome= $_SESSION['add_data']['nome'];
    $cognome = $_SESSION['add_data']['cognome'];
    $patente = $_SESSION['add_data']['patente'];
    $ruolo = $_SESSION['add_data']['role'];
    unset($_SESSION['add_data']);

} else {
    $msg = '';
    $nome =  $profilo['nome'];
    $cognome = $profilo['cognome'];
    $patente = $profilo['patente'];
    $ruolo = $profilo['role'];
}

$row = $stmt->fetch(PDO::FETCH_ASSOC);
#var_export($_SESSION);
#die();
?>

<html>

<head>
    <link rel="stylesheet" href="profilo.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="../../main.js"></script>
</head>

<body class="profilo">
<form method="post" action="modifica_foto.php?id=<?= $id ?>" enctype="multipart/form-data">

    <div class="card" >
        <div class="colore">

            <div class="immagine" style="margin-bottom: 40px">
                <?php if(file_exists("../foto/$id.png")): ?>
                    <img  id="pfp-img" class="img" src="../foto/<?= $id ?>.png" height="200px">
                <?php else: ?>
                    <img  id="pfp-img" class="img" src="../foto/profilo2.png" height="200px">
                <?php endif ?> &nbsp; &nbsp;
                <input id="image" name="image" accept="image/png" class="file" type="file" />
                <ion-icon name="pencil-outline"></ion-icon>
                <span class="material-icons" style="z-index: 1">delete</span>
            </div>



            <label class="title"><?=  $ruolo ?></label>
        </div>

        <div class="colors">
            <div class="contenuto">

                <label style="font-size: 27px;" >Nome:<input id="title" type="text" name="nome" size="10" placeholder="Nome" maxlength="255" value="<?= $nome?>" style="font-size: 23px; "/></label>

                <label style="font-size: 27px;" >Cognome:<input id="title" type="text" name="cognome" placeholder="Cognome" size="7" maxlength="255" value="<?= $cognome?>"style="font-size: 23px;"></label>

                <label style="font-size: 27px;" size="10" >Patente: <?= $patente?></label>
            </div>

            <div class="bottoni">
                <button class="btn" style="width: 90px;height: 30px" type="button" value="Annulla" onclick="history.back()">Annulla</button>
                <button class="btn" style="width: 90px;height: 30px" type="reset">Reset</button>
                <button class="btn" style="width: 90px;height: 30px" type="submit" value="Salva">Salva</button>
            </div>

        </div>

    </div>
    <form>
</body>
<script src="../../js/studente.js"></script>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</html>





