<?php

require "../../config.php";
require_once "../../config.php";

#var_export($_POST); die;

$username = $_POST['username'] ?? '';
$nome = $_POST['nome'] ?? '';
$cognome = $_POST['cognome'] ?? '';
#$patente = $_POST['patente'] ?? '';
$id = $_POST['id'] ?? 0;
$id_patente = $_POST['id_patente'] ?? 0;
#$id_docenti = $_POST['id_docenti'] ?? 0;

if ($id == '') $id = 0;
//var_dump($year);

if ($nome == '') {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'msg' => 'Some required data is missing',
        'nome' => $nome ,
        'cognome' => $cognome,
    ];
    header('location: /admin/ad_studente/edit.php?');
    die;
}

try {

    $stmt = $db-> prepare("
       UPDATE iscritti SET
        nome = :nome,
        cognome= :cognome,
        id_patente=:id_patente
        where id = :id
    ");

    $stmt->bindParam(':nome', $nome);
    $stmt->bindParam(':cognome', $cognome);
    #$stmt->bindParam(':patente', $patente);
    $stmt->bindParam(':id_patente', $id_patente);
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    $stmtb = $db->prepare("
        SELECT * from iscritti where id=:id    
    ");

    $stmtb->bindParam(':id', $id);
    $stmtb->execute();

    $row=$stmtb->fetch(PDO::FETCH_ASSOC);

    $id_users=$row['id_users'];

    $stmta = $db-> prepare("
       UPDATE users SET
        username=:username
        where id = :id_users
    ");

    $stmta->bindParam(':id_users', $id_users);
    $stmta->bindParam(':username', $username);
    $stmta->execute();


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/ad_studente/ad_studente.php');

?>



