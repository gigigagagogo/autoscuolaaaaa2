<?php

$id = $_GET['id'] ?? 0;

//$id = isset($_GET['id']) ? $_GET['id'] : 0;
/*
if (isset($_GET['id'])) {
    $id = $_GET['id']
} else {
    $id = 0;
}
*/

header('location: ad_studente.php');
#redirect non memorizza questa pagina
# in caso di errori commentare
#si può scrivere all'inizio. in caso di errore si viene ridirezionati comunque.

require "../../config.php";



try {


    $stmtb = $db->prepare("
        SELECT I.id, I.id_users from iscritti I where I.id=:id    
    ");
    $stmtb->bindParam(':id', $id);
    $stmtb->execute();

    $row=$stmtb->fetch(PDO::FETCH_ASSOC);
    $id=$row['id'];
    $id_users=$row['id_users'];

    $stmt = $db-> prepare("DELETE FROM iscritti where id  = :id");
    $stmt->bindParam(':id', $id);
    #$stmt->bindValue(':id', $id); //Fa dei controlli minori
    $stmt->execute();

    $stmta = $db-> prepare("
       DELETE FROM users where id = :id_users
    ");
    $stmta->bindParam(':id_users', $id_users);
    $stmta->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


?>