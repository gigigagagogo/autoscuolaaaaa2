<?php
require_once "../../config.php";

try {
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

$id=$_SESSION['user']['id'];



?>

<html>
<head>
    <link rel="stylesheet" href="contacts.css">
    <link rel="stylesheet" href="../../Home/nav.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>


<div class="inizio">
    <header id="prova">
        <a href="../index.php"><img src="../../assets/AutoscuolaBararu.png" alt="Logo" width="260px" class="logo"/></a>
        <div class="con">
            <div class="immagine">
                <?php if(file_exists("../foto/$id.png")): ?>
                    <?php if($_SESSION['user']['role']=='Admin'): ?>
                        <a style="height: 100%" href="../amministrazione/admin.php"><img style="height: 100%" id="pfp-img" class="img" src="../foto/<?= $id ?>.png" ></a>
                    <?php else: ?>
                        <a style="height: 100%" href="../profili/profilo.php"><img style="height: 100%" id="pfp-img" class="img" src="../foto/<?= $id ?>.png" ></a>
                    <?php endif ?>

                <?php else: ?>
                    <?php if($_SESSION['user']['role']=='Admin'): ?>
                        <a style="height: 100%" href="../amministrazione/admin.php"><img style="height: 100%" class="img" src="../foto/profilo2.png" ></a>
                    <?php else: ?>
                        <a style="height: 100%" href="../profili/profilo.php"><img style="height: 100%" class="img" src="../foto/profilo2.png" ></a>
                    <?php endif ?>
                <?php endif ?> &nbsp; &nbsp;
            </div>
        <a href="../logout.php"><img style="width: 50px;margin-right: -60px" src="../../assets/porta.png"></a>
        </div>

    </header>
</div>

<br>
<br>

<div class="main">

    <div class="testo">
        <h2 style="font-family: Chandas;margin-bottom: -25px">Autoscuola Bararu</h2><br>
        <div  class="mail">
            <ion-icon id="icone" name="mail-outline"></ion-icon>autoscuolabararu@brubi.it
        </div>
        <div class="call">
            <ion-icon id="icone" name="call-outline"></ion-icon>Tel: +39 351 688 3870
        </div>
        <div class="position">
            <ion-icon id="icone" name="earth-outline"></ion-icon>Via Cesare Battisti 34-Castiglione D/S(MN)
        </div>
        <div class="orario">
            <ion-icon id="icone" name="time-outline"></ion-icon>Orario di apertura ufficio:<br>
        </div>
        Da lunedi a Venerdi:<br>
        09:00-12:30/15:00-19:00
        <br>
        Sabato:<br>
        09:00-12:00

    </div>

    <div id="mappa" class="mapouter">
        <div class="gmap_canvas">
            <iframe id="gmap_canvas" src="https://maps.google.com/maps?q=Castiglione%20delle%20stiviere%20via%20cesare%20battisi%20n34&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            <style>.mapouter{position:relative;text-align:right;height:730px;width:730px;}</style>
            <style>.gmap_canvas {overflow:hidden;background:none!important;height:730px;width:730px;}</style>
        </div>
    </div>


</div>
<div class="fine">
    <header id="prova2">
        <img src="../../assets/AutoscuolaBararu.png" alt="Logo" width="260px" class="logo" style="position: relative; right: 15px"/>
        <ul class="menu2">
            <label class="informazioni">Castiglione delle Stiviere<br>Via Cesare Battisti, 34</label>
            <label class="informazioni">Indirizzo Email:<br>autoscuolabararu@brubi.it</label>
            <label class="informazioni">Numero di Telefono:<br>+39 351 688 3870</label>
        </ul>
    </header>
</div>


</body>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</html>
