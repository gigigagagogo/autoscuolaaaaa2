<?php
require_once "../../config.php";
require_once "../../authorized.php";
verify('Docente' || 'Studente' || 'Admin');
try {
    $id = intval($_GET['id']) ?? 0;

    $stmt = $db->prepare("
    SELECT U.username,I.nome,I.cognome,P.patente as patente,U.role FROM iscritti I 
    left join patenti P on P.id=I.id_patente
    left join users U on U.id=I.id_users
    where I.id=:id
    ");
    $stmt->bindParam(":id", $id);

    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}



?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Autoscuola Bararu</title>
    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon-32x32.png">
    <link rel="stylesheet" href="home.css">
    <link rel="stylesheet" href="../../Home/nav.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>



<div class="container">
    <div class="inizio">
        <header id="prova">
            <img src="../../assets/AutoscuolaBararu.png" alt="Logo" width="20%" class="logo"/>
                <div class="con">
                    <div class="immagine">
                        <?php if(file_exists("../foto/$id.png")): ?>
                            <?php if($_SESSION['user']['role']=='Admin'): ?>
                                <a style="height: 100%" href="../amministrazione/admin.php"><img style="height: 100%" id="pfp-img" class="img" src="../foto/admin2.png" ></a>
                            <?php else: ?>
                                <a style="height: 100%" href="../profili/profilo.php?id=<?= $id ?>"><img style="height: 100%" id="pfp-img" class="img" src="../foto/<?= $id ?>.png" ></a>
                            <?php endif ?>


                        <?php else: ?>
                            <?php if($_SESSION['user']['role']=='Admin'): ?>
                                <a style="height: 100%" href="../amministrazione/admin.php"><img style="height: 100%" class="img" src="../foto/admin2.png" ></a>
                            <?php else: ?>
                                <a style="height: 100%" href="../profili/profilo.php?id=<?= $id ?>"><img style="height: 100%" class="img" src="../foto/profilo2.png" ></a>
                            <?php endif ?>
                        <?php endif ?> &nbsp; &nbsp;
                    </div>
                    <a href="../logout.php"><img style="width: 50px;margin-right: -60px" src="../../assets/porta.png"></a>
                </div>
        </header>
    </div>
    <br>
    <br>
    <br>
    <br>


    <img class="macchinar" src="../../assets/macchinina.png"/>
    <div class="center" id="motivazionale">
        <label style="font-family: 'Dancing Script', cursive;">"Non importa cosa c'è sotto il cofano, importa chi c'è dietro al volante!"</label>
    </div>

    <div class="rettangolor"><a href="contacts.php"><h1><label>Dove trovarci</label></h1></a></div>

    <div class="rettangoloblu">
        <img src="../../assets/tipa.png"/>
        <div class="testo" >
            <label style="font-family: 'Gentium Plus', serif;font-size: 19px;">
                Leader nel settore delle patenti dal 1970, vanta un’alta <br>
                professionalità grazie al Know-how acquisito negli anni e <br>
                all’esperienza dei propri insegnanti. Dalle lezioni teoriche <br>
                in aula alle esercitazioni su computer fino alle guide con<br>
                istruttore di scuola guida e il conseguimento dell’abilitazione.<br>
            </label>
        </div>
    </div>

    <div class="rettangolo3">
        <div class="testo2">
            <label style="font-family: 'Gentium Plus', serif;font-size: 19px;">
                Lasciati "guidare" dalla nostra esperienza,<br>
                impara in modo consapevole i principi teorici<br>
                e pratici di un buon guidatore. L'autoscuola Bararu<br>
                a Castiglione D/S organizza corsi di preparazione<br>
                agli esami teorici. Vengono organizzati corsi pratici<br>
                di guida e tutte le pratiche necessarie per<br>
                chi deve prendere la patente
            </label>
        </div>
        <img src="../../assets/2im.png"/>
    </div>

    <div class="rettangoloblu">
        <img src="../../assets/tab.png"/>
        <div class="testo">

            <label style="font-family: 'Gentium Plus', serif;font-size: 19px;">
                Possibilitò di partecipazione online e in presenza, <br>
                lezioni online registrabili per rivederle tutte le volte<br>
                che si vuole, applicazione dedicata per ogni studente<br>
                con E-book e esercitazioni sui quiz.<br>
            </label>

        </div>
    </div>

    <div class="rettangolob"><h1><label>Contattaci al : +39 351 688 3870</label></h1></div>

    <div class="footer">
        <div class="sinistra">
            <h5>
                Contattaci info@brubi.it | +39 351 688 3870<br>
                Lavora con noi AutoscuolaBararu@brubi.it<br>
                Sede Operativa Cesare Battisti 34, Castiglione delle Stiviere(MN)
            </h5>
        </div>

        <div class="destra">
            <h5>
                Autoscuole Bararu<br>
                C.F e P.IVA 03709232562<br>
                ©2022 Copyright<br>
                Privacy Policy Cookie Policy
            </h5>
        </div>
    </div>


</div>


</body>
</html>
