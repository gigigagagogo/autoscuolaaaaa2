
<?php

require_once "../../config.php";
require "../../config.php";

$stmt = $db->prepare("
            SELECT * from patenti;
            ");
$stmt->execute();
?>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="register2.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<div class="container">
    <form method="post" action="register_r2.php" class="form">

        <nav>
            <ul class="menu">
                <li class="has-children"><a href="#"><span class="material-icons" >menu</span></a>
                    <ul class="sub-menu">
                        <li><a href="../../index.php">Home</a></li>
                        <li><a href="../../Home/Contattaci.php">Contattaci</a></li>
                        <li><a href="../../admin/login/login2.php">Login</a></li>
                    </ul>
                </li>
                <!--<h3 >Autoscuole<br>Bararu</h3>-->
            </ul>
        </nav>

        <div class="mezzo">
            <form method="post" action=register_r2.php" class="form">
                <div class="Customer">
                    <h4>CUSTOMER<br>REGISTER</h4>
                </div>
                <div class="contenuto">

                    <div class="form_input username">
                        <span style="color: black"; class="material-icons" id="omino" >person</span>
                        <input class="field" type="text" id="username" name="username" style="outline: none" placeholder="Username" size="30" >
                    </div>

                    <div class="form_input username">
                        <span style="color: black"; class="material-icons" id="omino" >badge</span>
                        <input class="field" type="text" id="nome" name="nome" placeholder="Nome" size="30" style="outline: none" >
                    </div>


                    <div class="form_input username">
                        <span style="color: black"; class="material-icons" id="omino" >badge</span>
                        <input class="field" type="text" id="cognome" name="cognome" style="outline: none" placeholder="Cognome" size="30" >
                    </div>


                    <div class="form_input username">
                        <span style="color: black"; class="material-icons" id="lucchetto">lock</span>
                        <input class="field" type="password" id="password" name="password" style="outline: none" placeholder="Password" size="30">
                    </div>

                    <div class="elements">
                        <table>
                            <tr>
                                <td>Patente:</td>
                                <td>
                                    <select name="id_patente" id="id_patente">
                                        <!--<option selected hidden>Seleziona una patente</option>-->
                                        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
                                            <option value="<?= $row['id'] ?>"><?= $row['patente'] ?></option>
                                        <?php endwhile ?>
                                    </select>
                                </td>
                            </tr>
                        </table>



                        <div class="log">
                            <input  class="btn-register" type="submit" value="Register" />
                        </div>
                </div>
            </form>
        </div>


</div>
</body>

</html>



