<?php
require_once "../config.php";
if(!isset($_SESSION['backto'])){
    $backto=$_SERVER['HTTP_REFERER'] ?? '';
    if($backto==''){
        $backto='login2.php';
    }
    $_SESSION['backto']=$backto;
}
?>

<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="login/login2.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>

<div class="container">
    <nav>
        <ul class="menu">
            <li class="has-children"><a href="#"><span class="material-icons" >menu</span></a>
                <ul class="sub-menu">
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../admin/login/login2.php">Login</a></li>
                    <li><a href="../admin/register/register4.php">Register</a></li>
                </ul>
            </li>
            <!--<h3 >Autoscuole<br>Bararu</h3>-->
        </ul>
    </nav>
    <div class="mezzo">
        <form method="post" action="pass_r.php" class="form">
            <div class="Customer">
                <h4>CUSTOMER<br>LOGIN</h4>
            </div>
            <div class="contenuto">
                <div class="form_input username">
                    <span style="color: black;" class="material-icons" id="omino" >person</span>
                    <input class="field" type="text" id="username" name="username" placeholder="Username" size="30" style="outline: none;">
                </div>

                <br>

                <div class="form_input username">
                    <span style="color: black; " class="material-icons" id="lucchetto">lock</span>
                    <input class="field" type="password" id="password" name="npassword" placeholder="Nuova Password" size="30" style="outline: none;">
                </div>

                <br>

                <div class="form_input username">
                    <span style="color: black; " class="material-icons" id="lucchetto">lock</span>
                    <input class="field" type="password" id="password" name="cpassword" placeholder="Conferma Password" size="30" style="outline: none;">
                </div>




                    <div  style="font-weight: bold;margin-top: 25px;mar">
                        <input  style="cursor: pointer" class="btn-login" type="submit" value="Conferma" />
                </div>
                <br><br>

            </div>
        </form>
    </div>


</div>
</body>

</html>

