function select_image(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            //$('#pim').attr('src', e.target.result);
            document.getElementById('pim').src = e.target.result;
            document.getElementById('pim').style.opacity = '1';
        };

        reader.readAsDataURL(input.files[0]);
    }
}